server {
    server_name  examenu.net;

    listen [::]:443 ssl http2;
    listen 443 ssl http2;
    ssl_certificate /etc/ssl/private/live/examenu.net/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/ssl/private/live/examenu.net/privkey.pem; # managed by Certbot
    ssl_protocols TLSv1.2 TLSv1.3;

    ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:ECDHE-RSA-AES128-GCM-SHA256:AES256+EECDH:DHE-RSA-AES128-GCM-SHA256:AES256+EDH:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4";
    ssl_prefer_server_ciphers on;
    ssl_session_cache shared:le_nginx_SSL:10m;
    ssl_dhparam /etc/ssl/certs/dhparam.pem;
    ssl_ecdh_curve secp384r1; 
    ssl_session_timeout 1440m;
    ssl_session_tickets off;


    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    location ^~ /.well-known/acme-challenge/ {
        allow all;
        root /var/lib/letsencrypt/;
        default_type "text/plain";
        try_files $uri =404;
    }

}
server {
    server_name  www.examenu.net;

    listen [::]:443 ssl http2;
    listen 443 ssl http2;
    ssl_certificate /etc/ssl/private/live/examenu.net/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/ssl/private/live/examenu.net/privkey.pem; # managed by Certbot
    ssl_protocols TLSv1.2 TLSv1.3;

    ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:ECDHE-RSA-AES128-GCM-SHA256:AES256+EECDH:DHE-RSA-AES128-GCM-SHA256:AES256+EDH:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4";
    ssl_prefer_server_ciphers on;
    ssl_session_cache shared:le_nginx_SSL:10m;
    ssl_dhparam /etc/ssl/certs/dhparam.pem;
    ssl_ecdh_curve secp384r1; 
    ssl_session_timeout 1440m;
    ssl_session_tickets off;


    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    location ^~ /.well-known/acme-challenge/ {
        allow all;
        root /var/lib/letsencrypt/;
        default_type "text/plain";
        try_files $uri =404;
    }




}
server {
    server_name  api.examenu.net;

    listen [::]:443 ssl http2;
    listen 443 ssl http2;
    ssl_certificate /etc/ssl/private/live/examenu.net/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/ssl/private/live/examenu.net/privkey.pem; # managed by Certbot
    ssl_protocols TLSv1.2 TLSv1.3;

    ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:ECDHE-RSA-AES128-GCM-SHA256:AES256+EECDH:DHE-RSA-AES128-GCM-SHA256:AES256+EDH:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4";
    ssl_prefer_server_ciphers on;
    ssl_session_cache shared:le_nginx_SSL:10m;
    ssl_dhparam /etc/ssl/certs/dhparam.pem;
    ssl_ecdh_curve secp384r1; 
    ssl_session_timeout 1440m;
    ssl_session_tickets off;


    location / {
        client_max_body_size 64M;
        add_header Access-Control-Allow-Origin *;
        proxy_pass   http://backend:3000;
    }

    location ^~ /.well-known/acme-challenge/ {
        allow all;
        root /var/lib/letsencrypt/;
        default_type "text/plain";
        try_files $uri =404;
    }

}

server {
    if ($host = examenu.net) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    server_name  examenu.net;
    listen       80;
    listen  [::]:80;
    return 404; 


}
server {
    if ($host = api.examenu.net) {
        return 301 https://$host$request_uri;
    } 


    server_name  api.examenu.net;
    listen       80;
    listen  [::]:80;
    return 404;


}
server {
    if ($host = www.examenu.net) {
        return 301 https://$host$request_uri;
    }


    server_name  www.examenu.net;
    listen       80;
    listen  [::]:80;
    return 404;


}
